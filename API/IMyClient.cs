﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocketClient
{
    interface IMyClient
    {
        Task Connect();
        Task Send(string message);
        Task BeginRecieve();
        Task Close();

        event Action<string> OnRecive;
    }
}
