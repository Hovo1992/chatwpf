﻿using System;
using System.Threading;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Text;

namespace WebSocketClient
{
    public class MyClient : IMyClient
    {
        public event Action<string> OnRecive;       
        
        #region Private variables

        ClientWebSocket _client;
        Uri _uri;

        #endregion

        #region Constructors

        public MyClient(Uri uri)
        {
            _client = new ClientWebSocket();
            _uri = uri;
        }

        #endregion

        #region Methods
        
        public WebSocketState state { get { return _client.State; } }

        public async Task Close()
        {
            await _client.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed By Client", CancellationToken.None);
        }
        public async Task Connect()
        {
            await _client.ConnectAsync(_uri, CancellationToken.None);          
        }

        public async Task Send(string message)
        {
            var data = Encoding.Unicode.GetBytes(message);
            ArraySegment<byte> segment = new ArraySegment<byte>(data);
            await _client.SendAsync(segment, WebSocketMessageType.Text, true, CancellationToken.None).ConfigureAwait(false);
        }

        public async Task BeginRecieve()
        {
            var buffer = new byte[256];
            while (true)
            {
                await _client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None).ConfigureAwait(false);
                string message = Encoding.Unicode.GetString(buffer);
                OnRecive(message);
            }            
        }
        #endregion
    }
}
