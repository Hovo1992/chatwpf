﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebSocketClient;

namespace ChatClientWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IMyClient _client;
       /// bool isConnected = false;
        public MainWindow()
        {
            InitializeComponent();

            Uri clientUri = new Uri("ws://localhost:8088");
            _client = new MyClient(clientUri);

            _client.OnRecive += Print;
            _client.Connect();
            _client.BeginRecieve();
        }
        
        private void Print(string message)
        {
            MessageArea.Text = message;
        }
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            _client.Send(MessageInput.Text);
        }
        private void MessageArea_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void MessageInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void ServerIP_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Port_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Name_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Ussers_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        //public void ConnectUser() 
        //{
        //    if (!isConnected)
        //    {
        //        Connect.Content= "Disconnect";
        //        isConnected = true;
        //    }
        //}
        //public void DisconnectUser()
        //{

        //    if (isConnected)
        //    {
        //        Connect.Content = "Connect";
        //        isConnected = false;
        //    }
        //}
        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            //if (isConnected)
            //{
            //    DisconnectUser();
            //}
            //else
            //{
            //    ConnectUser();
            //}
        }       
    }
}
